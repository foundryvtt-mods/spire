# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.13.0] - 2023-08-09
### Changed
- Different layout for fallout rolls.

### Fixed
- Fix missing Italian translation entries.

## [1.12.0] - 2023-08-08
### Added
- Fallout roll.
- Update verified Foundry VTT version to 11.307.

### Fixed
- Remove an unused translation key.

## [1.11.0] - 2023-06-27
### Added
- Refresh roll.
- Update verified Foundry VTT version to 11.302.

## [1.10.0] - 2023-06-26
### Added
- Stress roll.
- Contributors list.

## [1.9.1] - 2023-06-14
### Fixed
- Fix missing Italian translation entries.

## [1.9.0] - 2023-05-31
### Changed
- New action roll popup.

### Fixed
- Fix translation keys not compatible with Foundry VTT v11.

## [1.8.0] - 2023-05-28
### Added
- Compatibility with Foundry VTT v11.
- Background image.

## [1.7.2] - 2023-05-27
### Fixed
- Fix items descriptions not expanding when character sheet is rendered.

## [1.7.1] - 2023-05-01
### Fixed
- Fix armor calculation for total stress.

## [1.7.0] - 2023-05-01
### Added
- Stress tracker.
- Armor field.

### Changed
- Change style of class and durance fields.

## [1.6.1] - 2023-04-30
### Fixed
- Fix missing German translation entries.

## [1.6.0] - 2023-04-29
### Added
- German translation.

### Changed
- Update code for Foundry VTT v10.

## [1.5.1] - 2022-06-10
### Fixed
- Fix a typo in the French translation.

## [1.5.0] - 2022-05-17
### Added
- Italian translation.

## [1.4.0] - 2022-02-09
### Added
- Collapsible item summaries.

## [1.3.0] - 2022-02-07
### Added
- French translation.

### Fixed
- Fix Foundry version number in system manifest.

## [1.2.0] - 2022-01-26
### Added
- Tooltip for table item titles.

## [1.1.0] - 2022-01-22
### Added
- Translation support.
- Spanish translation.

## [1.0.1] - 2022-01-20
### Added
- MIT license.
- Highlight the highest die in a roll.

### Fixed
- Fix text overflow for table item titles.

## [1.0.0] - 2022-01-19
### Changed
- Full rewrite for 1.0.0.

## [0.2.0] - 2021-06-23
### Added
- Difficulty penalty for dice rolls.

## [0.1.0] - 2021-06-19
### Added
- Initial version.
