import { findChecked, getItemDescription } from "../helpers/spire-helpers.js";
import {
  spireActionRoll,
  spireStressRoll,
  spireRefreshRoll,
  spireFalloutRoll,
} from "../spire-roll.js";

export class SpireCharacterSheet extends ActorSheet {
  /**
   * IDs for items on the sheet that have been expanded.
   * @type {Set<string>}
   * @protected
   */
  _expanded = new Set();

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["spire", "sheet", "character"],
      template: "systems/spire/templates/actors/spire-character-sheet.html",
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);
    if (!this.options.editable) {
      return;
    }

    // Roll buttons
    html.find("#action-roll-button").click(() => {
      this._openActionRollPopup();
    });
    html.find("#stress-roll-button").click(() => {
      this._openStressRollPopup();
    });
    html.find("#refresh-roll-button").click(() => {
      this._openRefreshRollPopup();
    });
    html.find("#fallout-roll-button").click(() => {
      this._openFalloutRollPopup();
    });

    // Add item buttons
    html.find(".spire-item-create").click(this._onItemCreate.bind(this));

    // Edit item buttons
    html.find(".spire-item-edit").click((ev) => {
      const boxItem = $(ev.currentTarget).parents(".spire-table-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      item.sheet.render(true);
    });

    // Delete item buttons
    html.find(".spire-item-delete").click((ev) => {
      const boxItem = $(ev.currentTarget).parents(".spire-table-item");
      const item = this.actor.items.get(boxItem.data("itemId"));
      item.delete();
    });

    // Hide or show item description
    html
      .find(".spire-refresh .spire-table-item-title-label")
      .click((event) => this._onItemSummary(event));
    html
      .find(".spire-abilities .spire-table-item-title-label")
      .click((event) => this._onItemSummary(event));
    html
      .find(".spire-bonds .spire-table-item-title-label")
      .click((event) => this._onItemSummary(event));
    html
      .find(".spire-fallout .spire-table-item-title-label")
      .click((event) => this._onItemSummary(event));
    html
      .find(".spire-equipment .spire-table-item-title-label")
      .click((event) => this._onItemSummary(event));

    for (let itemId of this._expanded) {
      const boxItem = html.find(`[data-item-id="${itemId}"]`);
      const item = this.actor.items.get(itemId);
      this._expandItemDescription(boxItem, item);
      boxItem.toggleClass("expanded");
    }
  }

  /** @override */
  getData() {
    const data = super.getData();
    const actorData = this.actor.toObject(false);
    data.system = actorData.system;

    // Prepare items
    if (actorData.type == "character") {
      this._prepareCharacterItems(data);
    }

    return data;
  }

  _onItemCreate(event) {
    event.preventDefault();
    const header = event.currentTarget;
    const type = header.dataset.type;
    const data = duplicate(header.dataset);
    const name = `New ${type.capitalize()}`;
    const itemData = {
      name: name,
      type: type,
      data: data,
    };
    delete itemData.data["type"];
    return this.actor
      .createEmbeddedDocuments("Item", [itemData])
      .then((items) => items[0].sheet.render(true));
  }

  _expandItemDescription(boxItem, item, slide = false) {
    let itemDescription = getItemDescription(item);
    if (slide) {
      boxItem.append(itemDescription.hide());
      itemDescription.slideDown(200);
    } else {
      boxItem.append(itemDescription);
    }
  }

  _onItemSummary(event) {
    event.preventDefault();
    const boxItem = $(event.currentTarget).parents(".spire-table-item");
    const item = this.actor.items.get(boxItem.data("itemId"));
    if (boxItem.hasClass("expanded")) {
      let summary = boxItem.children(".spire-table-item-summary");
      summary.slideUp(200, () => summary.remove());
      this._expanded.delete(item.id);
    } else {
      this._expandItemDescription(boxItem, item, true);
      this._expanded.add(item.id);
    }
    boxItem.toggleClass("expanded");
  }

  _prepareCharacterItems(sheetData) {
    const actorData = sheetData.actor;

    // Initialize containers.
    const abilities = [];
    const bonds = [];
    const equipments = [];
    const fallouts = [];
    const knacks = [];
    const refreshes = [];

    // Iterate through items, allocating to containers
    for (let i of sheetData.items) {
      i.img = i.img || DEFAULT_TOKEN;

      if (i.type === "ability") {
        abilities.push(i);
      } else if (i.type === "bond") {
        bonds.push(i);
      } else if (i.type === "knack") {
        knacks.push(i);
      } else if (i.type === "equipment") {
        equipments.push(i);
      } else if (i.type === "fallout") {
        fallouts.push(i);
      } else if (i.type === "refresh") {
        refreshes.push(i);
      }
    }

    // Assign and return
    actorData.abilities = abilities;
    actorData.bonds = bonds;
    actorData.equipments = equipments;
    actorData.fallouts = fallouts;
    actorData.knacks = knacks;
    actorData.refreshes = refreshes;
  }

  _increaseStress(resistance_key, value) {
    var update_object = {};
    update_object.system = {};
    update_object.system.resistances = {};
    update_object.system.resistances[resistance_key] = {};
    update_object.system.resistances[resistance_key].stress =
      this.actor.system.resistances[resistance_key].stress + value;
    this.actor.update(update_object);
  }

  _decreaseStress(resistance_key, value) {
    var update_object = {};
    update_object.system = {};
    update_object.system.resistances = {};
    update_object.system.resistances[resistance_key] = {};
    update_object.system.resistances[resistance_key].stress =
      this.actor.system.resistances[resistance_key].stress -
      Math.min(value, this.actor.system.resistances[resistance_key].stress);
    this.actor.update(update_object);
  }

  async _openActionRollPopup() {
    let content = await renderTemplate(
      "systems/spire/templates/spire-roll-popup-action.html",
      this.actor.toObject(false)
    );

    new Dialog({
      title: `Action Roll`,
      content: content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: "Roll",
          callback: (html) => {
            let selectedSkill = findChecked(html, "action-roll-skill");
            let selectedDomain = findChecked(html, "action-roll-domain");

            let skill = this.actor.system.skills[selectedSkill].value;
            let skillLabel = this.actor.system.skills[selectedSkill].label;
            let domain = this.actor.system.domains[selectedDomain].value;
            let domainLabel = this.actor.system.domains[selectedDomain].label;

            let mastery = html.find('[name="action-roll-mastery"]')[0].checked;
            let difficulty = findChecked(html, "action-roll-difficulty");
            let help = findChecked(html, "action-roll-help");

            spireActionRoll(
              skill,
              skillLabel,
              domain,
              domainLabel,
              mastery,
              difficulty,
              help
            );
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize("Close"),
        },
      },
      default: "yes",
    }).render(true, {id: 'spireDialog'});
  }

  async _openStressRollPopup() {
    let content = await renderTemplate(
      "systems/spire/templates/spire-roll-popup-stress.html",
      this.actor.toObject(false)
    );

    new Dialog({
      title: `Stress Roll`,
      content: content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: "Roll",
          callback: (html) => {
            let resistance_key = findChecked(html, "stress-roll-resistance");
            let resistance = this.actor.system.resistances[resistance_key];
            let dice = findChecked(html, "stress-roll-dice");
            let modifier = findChecked(html, "stress-roll-modifier");
            let ignore_armor = html.find('[name="stress-roll-ignore-armor"]')[0]
              .checked;
            let roll_total = spireStressRoll(resistance.label, dice, modifier);

            roll_total.then((value) => {
              if (resistance_key != "blood" || ignore_armor) {
                this._increaseStress(resistance_key, value);
              } else {
                // Use armor first for blood stress
                let remaining_armor =
                  this.actor.system.resistances.armor.value -
                  this.actor.system.resistances.armor.stress;
                if (value <= remaining_armor) {
                  this._increaseStress("armor", value);
                } else {
                  this._increaseStress("armor", remaining_armor);
                  this._increaseStress(resistance_key, value - remaining_armor);
                }
              }
            });
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize("Close"),
        },
      },
      default: "yes",
    }).render(true, {id: 'spireDialog'});
  }

  async _openRefreshRollPopup() {
    let content = await renderTemplate(
      "systems/spire/templates/spire-roll-popup-refresh.html",
      this.actor.toObject(false)
    );
    new Dialog({
      title: `Refresh Roll`,
      content: content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: "Roll",
          callback: (html) => {
            let resistance_key = findChecked(html, "refresh-roll-resistance");
            let resistance = this.actor.system.resistances[resistance_key];
            let dice = findChecked(html, "refresh-roll-dice");
            let roll_total = spireRefreshRoll(resistance.label, dice);

            roll_total.then((value) => {
              this._decreaseStress(resistance_key, value);
            });
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize("Close"),
        },
      },
      default: "yes",
    }).render(true, {id: 'spireDialog'});
  }

  async _openFalloutRollPopup() {
    let content = await renderTemplate(
      "systems/spire/templates/spire-roll-popup-fallout.html",
      this.actor.toObject(false)
    );
    new Dialog({
      title: `Fallout Roll`,
      content: content,
      buttons: {
        yes: {
          icon: "<i class='fas fa-check'></i>",
          label: "Roll",
          callback: (html) => {
            let resistance_key = findChecked(html, "fallout-roll-resistance");
            let resistance = this.actor.system.resistances[resistance_key];
            let total_stress = this.actor.system.totalStress;
            let fallout = spireFalloutRoll(total_stress, resistance.label);

            fallout.then((value) => {
              if (value) {
                if (total_stress >= 2 && total_stress <= 4) {
                  // Minor fallout
                  this._decreaseStress(resistance_key, 3);
                } else if (total_stress >= 5 && total_stress <= 8) {
                  // Moderate fallout
                  this._decreaseStress(resistance_key, 5);
                } else if (total_stress >= 9) {
                  // Severe fallout
                  this._decreaseStress(resistance_key, 7);
                }
              }
            });
          },
        },
        no: {
          icon: "<i class='fas fa-times'></i>",
          label: game.i18n.localize("Close"),
        },
      },
      default: "yes",
    }).render(true, {id: 'spireDialog'});
  }
}
