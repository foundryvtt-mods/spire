export async function spireActionRoll(
  skill,
  skillLabel,
  domain,
  domainLabel,
  mastery,
  difficulty,
  help
) {
  // The amount of dice before difficulty is taken into account
  let base_dice_amount = 1;

  // The amount of dice after difficulty is taken into account
  let final_dice_amount = 0;

  // Penalty applied if amount of dice goes below 1 as a result of difficulty
  let difficulty_penalty = 0;

  if (skill) {
    base_dice_amount++;
  }
  if (domain) {
    base_dice_amount++;
  }
  if (mastery) {
    base_dice_amount++;
  }

  base_dice_amount += parseInt(help);

  final_dice_amount = base_dice_amount - parseInt(difficulty);

  if (final_dice_amount < 1) {
    difficulty_penalty = 1 - final_dice_amount;
    final_dice_amount = 1;
  }

  let roll = new Roll(`${final_dice_amount}d10`, {});
  roll.roll({ async: true });
  let roll_results = roll.terms[0].results;
  let roll_status = getSpireRollStatus(roll_results, difficulty_penalty);

  showActionRollMessage(
    roll_status,
    roll_results,
    skill,
    skillLabel,
    domain,
    domainLabel,
    mastery,
    difficulty,
    help,
    roll
  );
  ui.chat.scrollBottom();
}

export async function spireStressRoll(resistanceLabel, dice, modifier) {
  let roll = new Roll(`${dice}${modifier}`, {});
  await roll.roll();
  showStressRollMessage(roll, resistanceLabel, roll.total);
  return roll.total;
}

export async function spireRefreshRoll(resistanceLabel, dice) {
  let roll = new Roll(`${dice}`, {});
  await roll.roll();
  showRefreshRollMessage(roll, resistanceLabel, roll.total);
  return roll.total;
}

export async function spireFalloutRoll(total_stress, resistance_label) {
  let roll = new Roll(`d10`, {});
  await roll.roll();
  let fallout = roll.total < total_stress;
  let severity = "";

  if (total_stress >= 2 && total_stress <= 4) {
    severity = "SPIRE.Minor";
  } else if (total_stress >= 5 && total_stress <= 8) {
    severity = "SPIRE.Moderate";
  } else if (total_stress >= 9) {
    severity = "SPIRE.Severe";
  }

  showFalloutRollMessage(roll, fallout, severity, resistance_label);
  return fallout;
}

function getSpireRollStatus(roll_results, difficulty_penalty) {
  let sorted_rolls = roll_results
    .map((i) => i.result)
    .sort(function (a, b) {
      return b - a;
    });
  let result = sorted_rolls[0];

  let success_rate = 0;

  if (result == 1) {
    // Critical Failure
    success_rate = 0;
  } else if (result >= 2 && result <= 5) {
    // Failure
    success_rate = 1;
  } else if (result >= 6 && result <= 7) {
    // Partial Success
    success_rate = 2;
  } else if (result >= 8 && result <= 9) {
    // Success
    success_rate = 3;
  } else if (result == 10) {
    // Critical Success
    success_rate = 4;
  }

  success_rate -= difficulty_penalty;

  if (success_rate >= 4) {
    return "critical-success";
  } else if (success_rate == 3) {
    return "success";
  } else if (success_rate == 2) {
    return "partial-success";
  } else if (success_rate == 1) {
    return "failure";
  } else if (success_rate <= 0) {
    return "critical-failure";
  }

  return "error";
}

async function showActionRollMessage(
  roll_status,
  roll_results,
  skill,
  skillLabel,
  domain,
  domainLabel,
  mastery,
  difficulty,
  help,
  roll
) {
  let speaker = ChatMessage.getSpeaker();

  // Find the highest result
  var highest_result = roll_results.reduce(
    function (a, b) {
      return { result: Math.max(a["result"], b["result"]) };
    },
    { result: 0 }
  ).result;

  // Set all dice as inactive
  for (var roll_result of roll_results) {
    roll_result.active = false;
  }

  // Pick the highest die and set it as active
  for (var roll_result of roll_results) {
    if (roll_result.result === highest_result) {
      roll_result.active = true;
      break;
    }
  }

  let result = await renderTemplate(
    "systems/spire/templates/spire-roll-result-action.html",
    {
      roll_status: roll_status,
      roll_results: roll_results,
      skill: skill,
      skillLabel: skillLabel,
      domain: domain,
      domainLabel: domainLabel,
      mastery: mastery,
      difficulty: difficulty,
      help: help,
    }
  );

  let messageData = {
    speaker: speaker,
    content: result,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
    roll: roll,
  };

  ChatMessage.create(messageData);
}

async function showStressRollMessage(roll, resistance_label, roll_total) {
  let speaker = ChatMessage.getSpeaker();

  let result = await renderTemplate(
    "systems/spire/templates/spire-roll-result-stress.html",
    {
      resistance_label: resistance_label,
      roll_total: roll_total,
    }
  );

  let messageData = {
    speaker: speaker,
    content: result,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
    roll: roll,
  };

  ChatMessage.create(messageData);
}

async function showRefreshRollMessage(roll, resistance_label, roll_total) {
  let speaker = ChatMessage.getSpeaker();

  let result = await renderTemplate(
    "systems/spire/templates/spire-roll-result-refresh.html",
    {
      resistance_label: resistance_label,
      roll_total: roll_total,
    }
  );

  let messageData = {
    speaker: speaker,
    content: result,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
    roll: roll,
  };

  ChatMessage.create(messageData);
}

async function showFalloutRollMessage(roll, fallout, severity, resistance_label) {
  let speaker = ChatMessage.getSpeaker();

  let result = await renderTemplate(
    "systems/spire/templates/spire-roll-result-fallout.html",
    {
      fallout: fallout,
      severity: severity,
      resistance_label: resistance_label,
    }
  );

  let messageData = {
    speaker: speaker,
    content: result,
    type: CONST.CHAT_MESSAGE_TYPES.ROLL,
    roll: roll,
  };

  ChatMessage.create(messageData);
}
